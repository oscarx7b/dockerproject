
## Background

1. This container references jupyter/scipy-notebook with the addition of performing pip install from requirements.txt to add additional packages.
2. The purpose of this container is to allow students to work with python and Juypter notebook without messing around with python installs on localhost.

## Running container
For efficient working, we suggest to mount the the work folder in the container to a volume (-v) on the localhost in our example it is `$(pwd)/work`, but you can replace it somewhere else.  That's where one can place data files and notebooks. 

```
docker pull cyberassembly/jupyterlabwork
docker run -it --rm -p 8888:8888 -v $(pwd)/work:/home/jovyan/work  cyberassembly/jupyterlabwork
```

## Example Build container with different python packages


1. Download the source from [OSCAR GitLab Link](https://gitlab.com/oscarx7b/dockerproject/-/tree/master/jupyterlab)


2. Replacement the requirements.txt with the python package you need. For example for featuretools, replace requirements.txt with  requirements-featuretools.txt

```
cp requirements-featuretools.txt requirements.txt
docker build -t <image name> .

``

